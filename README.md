# Tag substitution grammar

This repository contains an 
[ANTLR4](https://www.antlr.org/) grammar definition
for a templating engine of sorts that is intended
for tag values substitution in corporate chat 
and/or email messages sent from CRM system.
Message templates are prepared by non tech-savvy users.
One can not impose some strict or complex grammar 
rules on such people (like in some templating engines 
intended for programmers).
So the parser has to be as "forgiving" as possible.
The same way most browsers silently ignore HTML 
errors and produce some more or less sensible output
from input HTML not conforming to standards.

## Grammar

The parser finds all the tags of the form `<tag name>`
inside plaintext message text. 
Tag start `<` and tag end `>` symbols can be escaped 
with `\`.

```html
Dear <FirstName>, happy to inform you that your order <OrderNumber> is on its way to you.

Cheers your <PersonalManagerName>
```

produces this tree
![Parsing tree example](example.png)

Further examples can be found in test folder.

See [Lexer](TagsLexer.g4) and [Parser](TagsParser.g4).
Parser rules are visualized [here](TagsParser.rrd.png).


## Testing the grammar

To see tokens for `input.txt` run
```bash
antlr4 TagsLexer.g4 && javac Tags*.java && grun Tags tokens -tokens input.txt
```

To see parsing tree for `input.txt` run
```bash
antlr4 TagsLexer.g4 && antlr4 TagsParser.g4 && javac Tags*.java && grun Tags message -gui input.txt
```

## Installing ANTLR
To install ANTLR4 on Linux follow official [instructions](https://github.com/antlr/antlr4/blob/master/doc/getting-started.md).

basically you have to 
* install JDK
* download ANTLR `.jar` file and place it into into `/usr/local/bin/` (or other folder of your choosing)
* add the following export and aliases into your shell startup file. For example for bash (adjust antlr `.jar` path and name as needed)
```bash
export CLASSPATH=".:/usr/local/bin/antlr-4.9.3-complete.jar:$CLASSPATH"
alias antlr4='java -Xmx500M -cp ".:/usr/local/bin/antlr-4.9.3-complete.jar" org.antlr.v4.Tool'
alias grun='java -Xmx500M -cp ".:/usr/local/bin/antlr-4.9.3-complete.jar" org.antlr.v4.gui.TestRig'
```

