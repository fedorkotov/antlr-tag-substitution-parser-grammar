parser grammar TagsParser;

options { tokenVocab=TagsLexer; }

eol: CR | CR LF | LF;
escaped_eol: ESCAPED_CR | ESCAPED_CR LF | ESCAPED_LF;

escaped_symbol: ESCAPED_TAGSTART 
              | ESCAPED_TAGEND 
              | ESCAPED_ESCAPE;

tag_name: (TEXT 
        | escaped_symbol )+;

tag: TAGSTART tag_name TAGEND;

message_line: 
            // line can include unmatched 
            // tag end symbols
            ((TEXT 
            | escaped_symbol 
            | TAGEND)*
            TAGEND )?
            // line can include tags
            // and plaintext
            (TEXT 
            | escaped_symbol 
            | tag)*
            // line can include unmatched
            // tag start symbols
            (TAGSTART
            (TEXT 
            | escaped_symbol 
            | TAGSTART)*)?;

message: message_line 
         ((eol|escaped_eol) message_line)* 
         (eol|escaped_eol)?;
