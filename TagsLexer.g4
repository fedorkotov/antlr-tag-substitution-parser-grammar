lexer grammar TagsLexer;

// inspired by https://stackoverflow.com/questions/31600514/what-is-the-best-way-to-escape-two-character-escape-sequences-in-an-antlr4-gramm

TAGSTART   : '<'-> pushMode(tagMode) ;
// more lexer command is used to glue
// escape symbol to the following
// symbol that it gives special meaning to
// so that we don't have to handle escaping
// in parser.
ESCAPE: '\\' -> more, pushMode(plaintextEscapeMode);
TEXT : ~('<'|'\\'|'\r'|'\n')+;
CR: '\r';
LF: '\n';

mode plaintextEscapeMode ;
ESCAPED_TAGSTART: '<' -> popMode ;
ESCAPED_TAGEND: '>' -> popMode ;
ESCAPED_ESCAPE: '\\' -> popMode ;
ESCAPED_TEXT : ~('<'|'>'|'\\'|'\r'|'\n') -> type(TEXT), popMode;
// Tag substitution does not require 
// special treatment of escaped end of lines
// (as opposed to ends of lines not preceded 
// by '\' symbol).
// But there seems to be no elegant way 
// of reversing 'more' lexer command 
// (see ESCAPE rule above)
ESCAPED_CR: '\r' -> popMode;
ESCAPED_LF: '\n' -> popMode;

mode tagMode ;
TAGEND : '>' -> popMode ;
TAG_ESCAPE: '\\' -> more, pushMode(tagEscapeMode);
TAGNAME : ~('>'|'\\'|'\r'|'\n')+ -> type(TEXT);
TAG_CR: '\r' -> type(CR), popMode;
TAG_LF: '\n' -> type(LF), popMode;

mode tagEscapeMode ;
TAG_ESCAPED_TAGSTART: '<' -> type(ESCAPED_TAGSTART), popMode ;
TAG_ESCAPED_TAGEND: '>' -> type(ESCAPED_TAGEND), popMode ;
TAG_ESCAPED_ESCAPE: '\\' -> type(ESCAPED_ESCAPE), popMode ;
TAG_ESCAPED_TAGNAME : ~('<'|'>'|'\\'|'\r'|'\n') -> type(TEXT), popMode;
TAG_ESCAPED_CR: '\r' -> type(ESCAPED_CR), popMode;
TAG_ESCAPED_LF: '\n' -> type(ESCAPED_LF), popMode;

